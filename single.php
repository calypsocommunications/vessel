<?php get_header(); ?>

<div class="Strip InteriorHeader ">
  <div class="InteriorHeader-top u-responsivePadding">
    <div class="SectionContainer">
      <a href="/"><svg class="BwLogo icon icon-VSSL-logo-1color"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-1color"></use></svg></a>


        <a href="/">
          <div class="BackToHome">
            <svg class="icon icon-VSSL-house"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-house"></use></svg>
          </div>
        </a>

  <a href="/news">
        <div class="CloseModalNavButton CloseModalNavButton--text CloseModalNavButton--news">
          <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
          <span>Back</span>
        </div>

      </a>
    </div>
  </div>


</div>

<div class="Strip Strip--yellowTop  SingleContent  u-responsivePadding">
  <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
    <div class="PrimaryContent">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

          <header class="ArticleHeader">

            <div class="EntryMeta">
              <span><time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('M. d, Y'); ?></time></span><span>, <?php the_category(', '); ?></span>
            </div> <!-- /EntryMeta -->

            <h1 class="SingleTitle" itemprop="headline"><?php the_title(); ?></h1>

            <?php the_post_thumbnail('crop-900-200'); ?>

          </header> <!-- /ArticleHeader -->

          <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
            <?php the_content(); ?>
          </section> <!-- /EntryContent -->

          <footer class="ArticleFooter">

            <?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>

          </footer> <!-- /article footer -->

          <?php comments_template(); // comments should go inside the article element ?>

        </article> <!-- /article -->

      <?php endwhile; else : ?>

        <article class="PostNotFound">
          <header class="ArticleHeader">
            <h1><?php _e("Oops, Post Not Found!", "flexdev"); ?></h1>
          </header>
          <section class="EntryContent">
            <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?></p>
          </section>
          <footer class="ArticleFooter">
            <p><?php _e("This is the error message in the single.php template.", "flexdev"); ?></p>
          </footer>
        </article>

      <?php endif; ?>

    </div> <!-- /PrimaryContent -->



  </main>
</div> <!-- /Strip-->

<!-- Browse News -->
<div class="Strip u-responsivePadding">
  <div class="SectionContainer">
    <div class="BrowseNews">
      <p>Browse News</p>
      <div class="StyledSelect">
        <?php wp_dropdown_categories( 'show_option_none=Topic' ); ?>
        <script type="text/javascript">
        <!--
        var dropdown = document.getElementById("cat");
        function onMyCatChange() {
          if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
          }
        }
        dropdown.onchange = onMyCatChange;
        -->
        </script>
      </div>

      <div class="StyledSelect">
        <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
          <option value=""><?php echo esc_attr( __( 'Date' ) ); ?></option>
          <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option' ) ); ?>
        </select>
      </div>

      <div class="LinkButton LinkButton--reversed LinkButton--reversedWhite"><a href="/news">All Articles</a></div>
    </div> <!-- /BrowseNews -->
  </div> <!-- /SectionContainer -->
</div>
<!-- /Browse News -->

<?php get_template_part( 'parts/lowercta' ); ?>

<div class="Strip Strip--yellowTopThin NewsFooter">
  <div class="SectionContainer cf">
    <div class="">
      <a href="/">
        <div class="CloseModalNavButton--iconOnly">
          <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
        </div>
      </a>
      <span class="SiteCopyright">Copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved</span>
    </div> <!-- /class_name -->
  </div> <!-- /SectionContainer -->
</div>


<?php get_footer(); ?>
