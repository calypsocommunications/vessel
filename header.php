<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <?php // force Internet Explorer to use the latest rendering engine available ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php // mobile meta (hooray!) ?>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <?php // icons & favicons ?>
  <?php // Using REAL FAVICON GENERATOR? Put generated code in file below + uncomment admin favicon setup (in admin.php) ?>
  <?php // get_template_part( 'favicon-head' ); ?>

  <?php // other html head stuff (before WP/theme scripts are loaded) ?>

  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:700|Roboto:300,400,900" rel="stylesheet">

  <!-- wordpress head functions -->
  <?php wp_head(); ?>
  <!-- end of wordpress head -->

  <?php // drop Google Analytics here ?>
  <meta name="google-site-verification" content="K7ty-7BArzNnlzX3OSW5AS-XHeEqiJCVtzIfQah_RBU" />
  <?php // end Analytics ?>

</head>

<body <?php body_class(pretty_body_class()); ?> itemscope itemtype="http://schema.org/WebPage">
  <!--[if lt IE 11]><p class="browserwarning">Your browser is <strong>very old.</strong> Please <a href="http://browsehappy.com/">upgrade to a different browser</a> to properly browse this web site.</p><![endif]-->

  <div class="Strip u-toplayer">


    <header class="PageHeader" role="banner" itemscope itemtype="http://schema.org/WPHeader">

      <div class="SectionContainer cf">

        <div class="SiteLogo" itemscope itemtype="http://schema.org/Organization">
          <a href="/" rel="nofollow">
            <svg class="icon icon-VSSL-logo-V-color"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-V-color"></use></svg>
          </a>
        </div>

        <div class="SiteLogoText" itemscope itemtype="http://schema.org/Organization">
          <a href="/" rel="nofollow">
            <svg class="icon icon-VSSL-logo-vessel-color"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-vessel-color"></use></svg>
          </a>
        </div>

        <div id="StickyMenuLink" class="StickyMenuLink">
          <span id="StickyMenuLink-menu" class="Show StickyMenuLink-menu"><svg class="icon icon-VSSL-sloppyburger"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-sloppyburger"></use></svg></span>
          <span id="StickyMenuLink-x" class="Hide StickyMenuLink-x"><svg class="icon icon-VSSL-closeX"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-closeX"></use></svg></span>
          MENU
        </div>


      </div> <!-- /SectionContainer -->

      <div class="PopdownMenu" id="PopdownMenu">
        <div class="SectionContainer cf">
          <nav class="TertiaryMenu" role="navigation">
            <?php fdt_nav_menu( 'tertiary-nav', 'TertiaryMenu' ); // Adjust using Menus in WordPress Admin -- REMOVE if not using ?>
          </nav>

          <!-- Search form -->
          <form id="search-form-popup" role="search" method="get" class="search-form-popup mfp-hide" action="<?php echo home_url( '/' ); ?>">
            <label>
              <span class="visuallyhidden"><?php echo _x( '', 'label' ) ?></span>
              </label>
              <div class="SearchInputWrap">


              <input type="search" class="search-field searchinput" placeholder="<?php echo esc_attr_x( 'What are you looking for?', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" id="search" title="<?php echo esc_attr_x( 'Search...', 'label' ) ?>" />
                </div>
              <button type="submit" id="searchsubmit" class="searchsubmit">Search</button>
            </form>

            <nav class="MainMenuWrap" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
              <ul class="MainMenu">
                <li><a href="#section1">What We Do</a></li>
                <li><a href="#section2">Our Process</a></li>
                <li><a href="#section3">Quality Assurance</a></li>
                <li><a href="#section4">The Vessel Advantage</a></li>
                <li><a href="#section5">Spotlight</a></li>
              </ul>
              <?php // fdt_nav_menu( 'main-nav', 'MainMenu' ); // Adjust using Menus in WordPress Admin ?>
            </nav>
          </div>
        </div>

      </header> <!-- /PageHeader -->



    </div>
