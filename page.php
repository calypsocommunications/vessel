<?php get_header(); ?>

<!-- Banner -->
<div  class="Strip HeroBanner Strip--afterBanner">
  <div class="HeroSlider">
    <?php if( have_rows('slides') ): ?>
      <?php while( have_rows('slides') ): the_row(); ?>

        <div class="HeroSlider-slide" style="background-image:url('<?php echo get_sub_field("slide"); ?>')">
          <?php if( get_sub_field('overlay_text') ) {
            echo '<div class="HeroSlider-text u-verticalCenter"><span></span><p class="">' . get_sub_field('overlay_text') . '</p><span></span></div>';
          }?>
        </div>

      <?php endwhile; ?>
    <?php endif; ?>
  </div>
  <div class="animated infinite bounce HeroBanner-arrow">
    <a href="#section1">Scroll<br />
      <svg class="icon icon-VSSL-arrow-down"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-down"></use></svg>
    </a>
  </div>
</div>

<!-- Section1 -->
<div id="section1" class="Strip Section Strip--yellowTop u-responsivePadding">
  <div class="SectionContainer">
    <h2 class="SectionTitle">What We Do</h2>
    <div class="u-innercontent"><?php echo get_field('wwd_intro'); ?></div>
    <?php get_template_part( 'parts/wwdcards' ); ?>
  </div>
</div>

<div class="Strip Strip--divider"></div>

<!-- Section2 -->
<div id="section2" class="Strip Section u-responsivePadding">
  <h2 class="SectionTitle">Our Process</h2>
  <div class="SectionContainer">
    <?php echo get_field('op_intro'); ?>
    <?php get_template_part( 'parts/opcards' ); ?>
  </div>
</div>

<!-- Section3 -->
<div id="section3" class="Strip Section Strip--bluePattern Strip--yellowTop">
  <div class="SectionContainer">
    <h2 class="SectionTitle">Quality Assurance</h2>
    <div class="QASlider">

      <?php if( have_rows('qa_slider') ): ?>
        <?php while( have_rows('qa_slider') ): the_row(); ?>

          <div class="QASlider-slide">
            <p><?php echo get_sub_field('qa_slide'); ?></p>
          </div>

        <?php endwhile; ?>
      <?php endif; ?>

    </div> <!-- / -->
  </div> <!-- /SectionContainer -->
</div>

<!-- Section4 -->
<div id="section4" class="Strip Section Strip--yellowTop">
  <div class="SectionContainer">
    <span id="CTALINK" class="CTALINK"></span>
    <h2 class="SectionTitle">The Vessel Advantage</h2>
      <?php echo get_field('va_intro'); ?>
    <?php get_template_part( 'parts/flipcard' ); ?>
  </div>
</div>

<!-- Section5 -->
<div id="section5" class="Strip Section Strip--yellowTop u-responsivePadding">
  <div class="SectionContainer">
    <h2 class="SectionTitle">Latest News</h2><br />

    <?php get_template_part( 'parts/news' ); ?>
  </div>
</div>

<!-- SectionCTA -->
<div id="section6" class="Strip Section Strip--yellowTop Strip--lowerCTA u-responsivePadding" style="background-image:url(<?php echo get_field('lower_cta_image'); ?>)">

  <div class="SectionContainer">
    <?php if( get_field('lower_cta_text') ) {
      echo '<p>' . get_field('lower_cta_text') . '</p>';
    }?>

  </div>
</div>

<div class="Strip Strip--divider Strip-logowall">
    <?php get_template_part( 'parts/logowall' ); ?>
</div>

<?php get_footer(); ?>
