<?php
/*
Template Name: Blog Main Page
*/
?>

<?php get_header(); ?>

<div class="Strip InteriorHeader">
  <div class="InteriorHeader-top u-responsivePadding">
    <div class="SectionContainer">
      <a href="/"><svg class="BwLogo icon icon-VSSL-logo-1color"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-1color"></use></svg></a>
      <a href="/">
        <div class="CloseModalNavButton CloseModalNavButton--text CloseModalNavButton--news">
          <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
          <span>Back</span>
        </div>
      </a>
    </div>
  </div>

  <div class="NewsBanner" style="background-image:url(<?php echo get_field('news_banner'); ?>)";>
    <span class="u-verticalCenter" style="width:100%;"><h1 class="MainTitle">Blog</h1></span>
  </div>

</div>

<div class="Strip  Strip--yellowTop  NewsContent  u-responsivePadding">
  <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
    <div class="PrimaryContent">

      <?php
      $temp = $wp_query;
      $wp_query = null;
      $wp_query = new WP_Query(
      array(
        'posts_per_page' => 3,
        'paged' => $paged
      )
    );
    ?>
<div class="AjaxInitialLoad">
    <?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
        <article <?php post_class('BlogIntroWrap'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
          <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
            <a href="<?php the_permalink();?>"> <?php the_post_thumbnail( 'crop-news' ); ?></a>
            <h4 itemprop="headline"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
            <div class="EntryMeta">
              <span><time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('M. d, Y');?></time></span><span>, <?php the_category(', '); ?></span>
            </div> <!-- /EntryMeta -->
            <p><?php echo fdt_excerpt(120); ?><a class="MoreLink" href="<?php the_permalink();?>">Read More <svg class="icon icon-VSSL-arrow-right"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-right"></use></svg> </a></p>
          </section> <!-- /EntryContent -->
        </article> <!-- /article -->
    <?php endwhile; endif; ?>
</div>
    <!-- <?php echo do_shortcode('[ajax_load_more post_type="post" offset="3" posts_per_page="3" max_pages="0" images_loaded="true"]'); ?> -->

    <?php $wp_query = null; $wp_query = $temp; ?>
    <?php wp_reset_postdata(); ?>

  </div> <!-- /PrimaryContent -->

</main>
</div> <!-- /Strip -->

<div class="Strip u-responsivePadding">
  <div class="SectionContainer">
    <div class="BrowseNews">
      <p>Browse News</p>
      <div class="StyledSelect">
        <?php wp_dropdown_categories( 'show_option_none=Topic' ); ?>
        <script type="text/javascript">
        <!--
        var dropdown = document.getElementById("cat");
        function onMyCatChange() {
          if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
          }
        }
        dropdown.onchange = onMyCatChange;
        -->
        </script>
      </div>

      <div class="StyledSelect">
        <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
          <option value=""><?php echo esc_attr( __( 'Date' ) ); ?></option>
          <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option' ) ); ?>
        </select>
      </div>

      <div class="LinkButton LinkButton--reversed LinkButton--reversedWhite"><a href="/news">All Articles</a></div>
    </div> <!-- /BrowseNews -->
  </div> <!-- /SectionContainer -->
</div>

<div class="Strip NewsCTA">
  <div class="SectionContainer">
    <div class="class_name">

    </div> <!-- /class_name -->
  </div> <!-- /SectionContainer -->
</div>

<div class="Strip Strip--yellowTopThin NewsFooter">
  <div class="SectionContainer cf">
      <a href="/">
        <div class="CloseModalNavButton--iconOnly">
          <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
        </div>
      </a>
      <span class="SiteCopyright">Copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved</span>
  </div> <!-- /SectionContainer -->
</div>

<?php get_footer(); ?>
