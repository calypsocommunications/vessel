<?php
/*
Template Name: Interior Page
*/
?>

<?php get_header(); ?>

<div class="Strip InteriorHeader">
  <div class="InteriorHeader-top u-responsivePadding">
    <div class="SectionContainer">
      <a href="/"><svg class="BwLogo icon icon-VSSL-logo-1color"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-1color"></use></svg></a>
      <a href="/">
        <div class="CloseModalNavButton CloseModalNavButton--text CloseModalNavButton--news">
          <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
          <span>Back</span>
        </div>
      </a>
    </div>
  </div>

  <div class="InteriorBanner" style="background-image: url(<?php if (current_theme_supports( 'post-thumbnails' ) && has_post_thumbnail( $post->ID )) {
  $page_bg_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  $page_bg_image_url = $page_bg_image[0]; // this returns just the URL of the image
  echo $page_bg_image_url;
} ?> );">
        <h1 class="MainTitle  u-verticalCenter"><?php the_title(); ?></h1>
  </div>

  <div class="Strip  Strip--yellowTop  InteriorContent">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <section class="EntryContent  cf u-responsivePadding">
          <?php the_content(); ?>
        </section> <!-- /EntryContent -->

      <?php endwhile; endif; // END main loop (if/while) ?>

      <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

    </main>
  </div> <!-- /Strip-->

  <?php get_template_part( 'parts/lowercta' ); ?>

  <div class="InteriorFooter">
    <div class="SectionContainer cf">
      <a href="/">
      <div class="close-ModalContent<?php echo $modalinnercount; ?>  CloseModalNavButton CloseModalNavButton--iconOnly"> <!-- class name must match id above, ie. close-IdName -->
        <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
      </div>
    </a>
      <span class="ModalCopyright">Copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved</span>
    </div>
  </div>


<?php get_footer(); ?>
