<?php
/* Welcome to FlexDev Theme
This is the core FlexDev theme file. A bunch of random important stuff is in here.
Other more commonly edited / used stuff is found in single files.
*/

/**
 * CONTENTS
 *
 * CLEAN UP ------- #clean
 * Flex Dev Go Time.................fire up key filteres and functions
 * Head clean up....................getting rid of junk WP generates
 *
 *
 * THEME SUPPORT ------ #theme
 * Theme Support....................activating WP functions for theme functionality
 *
 *
 * MENUS AND NAVIGATION ------- #nav
 * Menu register....................register / create nav menus
 * Menu function....................custom function for easy menu creation
 * Parent classes...................add parent classes to menus
 *
 *
 * CONTENT RELATED FUNCTIONS AND FIXES ------ #content
 * Excerpt clean up.................custom excerpt length and remove read more link
 * fdt_excerpt() function.........on the fly (multiple) custom excerpt length
 * Responsive video.................add wrapper class to enable responsive video
 * Widget area......................setup widgetized areas
 * Image P tag......................remove p tag around images
 *
 *
 * OTHER STUFF ------- #other
 * Better body classes..............default WP body classes suck
 */



/*********************************************
LAUNCH AND CLEAN - #clean
*********************************************/

function flexdev_go_time() {
  // launching operation cleanup
  add_action( 'init', 'fdt_head_cleanup' );
  // remove WP version from RSS
  add_filter( 'the_generator', 'fdt_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'fdt_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'fdt_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'fdt_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'fdt_scripts_and_styles', 999 );

  // launching this stuff after theme setup
  fdt_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'fdt_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'fdt_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'fdt_excerpt_more' );

} /* end fdt go time */

// let's get this party started
add_action( 'after_setup_theme', 'flexdev_go_time' );


function fdt_head_cleanup() {
  // category feeds
  // remove_action( 'wp_head', 'feed_links_extra', 3 );
  // post and comment feeds
  // remove_action( 'wp_head', 'feed_links', 2 );
  // EditURI link
  remove_action( 'wp_head', 'rsd_link' );
  // windows live writer
  remove_action( 'wp_head', 'wlwmanifest_link' );
  // previous link
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
  // start link
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
  // links for adjacent posts
  remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
  // WP version
  remove_action( 'wp_head', 'wp_generator' );
  // remove WP version from css
  add_filter( 'style_loader_src', 'fdt_remove_wp_ver_css_js', 9999 );
  // remove Wp version from scripts
  add_filter( 'script_loader_src', 'fdt_remove_wp_ver_css_js', 9999 );

} /* end fdt head cleanup */

// remove WP version from RSS
function fdt_rss_version() { return ''; }

// remove WP version from scripts
function fdt_remove_wp_ver_css_js( $src ) {
  if ( strpos( $src, 'ver=' ) )
    $src = remove_query_arg( 'ver', $src );
  return $src;
}

// remove injected CSS for recent comments widget
function fdt_remove_wp_widget_recent_comments_style() {
  if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
    remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
  }
}

// remove injected CSS from recent comments widget
function fdt_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
  }
}

// remove injected CSS from gallery
function fdt_gallery_style($css) {
  return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}






/**************************************************
THEME SUPPORT - #theme
**************************************************/

// Adding WP 3+ Functions & Theme Support
function fdt_theme_support() {

  // wp thumbnails (sizes handled in functions.php)
  add_theme_support( 'post-thumbnails' );

  // title tags
  add_theme_support( 'title-tag' );

  // default thumb size
  set_post_thumbnail_size( 150, 150, true );

  // HTML5 support
  add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

  // rss thingy
  add_theme_support('automatic-feed-links');

  // to add header image support go here: http://themble.com/support/adding-header-background-image-support/

  // adding post format support
  /*add_theme_support( 'post-formats',
    array(
      'aside',             // title less blurb
      'gallery',           // gallery of images
      'link',              // quick link to other site
      'image',             // an image
      'quote',             // a quick quote
      'status',            // a Facebook like status update
      'video',             // video
      'audio',             // audio
      'chat'               // chat transcript
    )
  ); */

  // wp menus
  add_theme_support( 'menus' );

} /* end fdt theme support section */




/***********************************************
MENUS & NAVIGATION - #nav
************************************************/

// setup wp_nav_menu stuff.............

// registering wp3+ menus
register_nav_menus(
  array(
    'main-nav' => 'Primary Menu',           // main nav menu
    'tertiary-nav' => 'Tertiary Menu',      // tertiary nav menu
    'footer-nav' => 'Footer Menu'           // secondary nav (in footer)
  )
);

/* usage: <?php fdt_nav_menu( 'main-nav', 'menu-main' ); ?> */
function fdt_nav_menu( $theme_location, $class ){
  if( has_nav_menu( $theme_location ) ) {
    $menu = wp_nav_menu( array(
    'container' => false,
    'theme_location'  => $theme_location,
    'menu_class'  => $class . '  nav  cf',
    'echo'  => 0,
    ) );
  }
  echo $menu;
}

// ADD A PARENT CLASS for wp_list_pages (usually only needed for drop down navigation)
function add_parent_class( $css_class, $page, $depth, $args )
{
  if ( ! empty( $args['has_children'] ) )
    $css_class[] = 'parent-menu-item';
  return $css_class;
}
add_filter( 'page_css_class', 'add_parent_class', 10, 4 );


// ADD A PARENT CLASS for wp_nav_menu (usually only needed for drop down navigation)
function parent_menu_css_class( $classes, $item, $args ) {
  $menu_items = wp_get_nav_menu_items( $args->menu );
  if ( ! $menu_items && $args->theme_location && ( $locations = get_nav_menu_locations() ) && isset( $locations[ $args->theme_location ] ) ) :
  $menu = wp_get_nav_menu_object( $locations[ $args->theme_location ] );
  $menu_items = wp_get_nav_menu_items( $menu->term_id );
  endif;
  foreach ( $menu_items as $menu_item ) :
    if ( $menu_item->menu_item_parent == $item->ID ) :
      $classes[] = 'parent-menu-item';
      break;
    endif;
  endforeach;
  return $classes;
 }
add_filter( 'nav_menu_css_class', 'parent_menu_css_class', 10, 3 );





/***************************************************
CONTENT RELATED FUNCTIONS AND FIXES - #content
****************************************************/

// custom excerpt length **************
function fdt_custom_excerpt_length( $length ) {
  return 65;
}
add_filter( 'excerpt_length', 'fdt_custom_excerpt_length', 999 );


// This removes the annoying […] to a Read More link
function fdt_excerpt_more($more) {
  global $post;
  // edit here if you like
  return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="Read '.get_the_title($post->ID).'"><span>Read more &raquo;</span></a>';
}


// ********** FlexDev custom excerpt length ********************
// USAGE: echo fdt_excerpt(25);
function fdt_excerpt($charlength) {
  $excerpt = strip_tags(get_the_content());
  $charlength++;
  $excerpt = preg_replace( '/\[[^\]]+\]/', '', $excerpt );
  if(strlen($excerpt)>$charlength) {
  $subex = substr($excerpt,0,$charlength-5);
  $exwords = explode(" ",$subex);
  $excut = -(strlen($exwords[count($exwords)-1]));
  if($excut<0) {
    echo substr($subex,0,$excut);
  } else {
    echo $subex;
  }
    echo " ";
  } else {
    echo $excerpt;
  }
}


// Make Read More link go to top of page (not an anchor link)
function remove_more_jump_link($link) {
  $offset = strpos($link, '#more-');
  if ($offset) {
    $end = strpos($link, '"',$offset);
  }
  if ($end) {
    $link = substr_replace($link, '', $offset, $end-$offset);
  }
  return $link;
  }
add_filter('the_content_more_link', 'remove_more_jump_link');



/************ RESPONSIVE VIDEOS *******************/
add_filter('embed_oembed_html', 'my_embed_oembed_html', 99, 4);
function my_embed_oembed_html($html, $url, $attr, $post_id) {
  return '<div class="EmbedContainer">' . $html . '</div>';
}





/************* WIDGETIZED AREAS ********************/

// Widgetizes Areas
function fdt_register_sidebars() {
  register_sidebar(array(
    'id' => 'sidebar',
    'name' => 'Sidebar',
    'description' => 'The primary sidebar.',
    'before_widget' => '<div id="%1$s" class="widget  cf  %2$s"><div class="widget-wrap">',
    'after_widget' => '</div></div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  // register_sidebar(array(
  //  'id' => 'sidebar-home',
  //  'name' => 'Home Sidebar',
  //  'description' => 'The sidebar on the home page.',
  //  'before_widget' => '<div id="%1$s" class="widget  cf  %2$s"><div class="widget-wrap">',
  //  'after_widget' => '</div></div>',
  //  'before_title' => '<h4 class="widgettitle">',
  //  'a
  //  fter_title' => '</h4>',
  // ));

} // don't remove this bracket!



// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function fdt_filter_ptags_on_images($content){
  return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}



/***************************************************
OTHER STUFF - #other
****************************************************/
// page template name body class - removes the stuff before and the .php after the template name
function my_short_page_template_body_classes( $classes ){
  if( is_page() ){
    $template = get_page_template_slug(); // returns an empty string if it's loading the default template (page.php)
    if( $template === '' ){
      $classes[] = 'default-page';
    } else {
      $classes[] = sanitize_html_class( str_replace( '.php', '', $template ) );
    }
  }
  return $classes;
}
add_filter( 'body_class', 'my_short_page_template_body_classes' );
// give us some nice body class names please
function pretty_body_class() {
  $term = get_queried_object();

  if (is_single()) {
    $cat = get_the_category();
  }

  if(!empty($cat)) {
    return $cat[0]->slug;
  } elseif(isset($term->slug)) {
    return $term->slug;
  } elseif(isset($term->post_name)) {
    return $term->post_name;
  } else {
    return;
  }
}



?>
