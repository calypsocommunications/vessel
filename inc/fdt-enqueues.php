<?php
/*********************************************
SCRIPTS & ENQUEUING - #enque
**********************************************/

// loading modernizr, jquery, reply script and any custom scripts for this project
function fdt_scripts_and_styles() {

  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around IE stylesheet the WordPress way

  if (!is_admin()) {

    // modernizr (without media query polyfill)
    wp_register_script( 'flexdev-modernizr', get_stylesheet_directory_uri() . '/assets/js/modernizr.custom.min.js', array(), '2.6.2', false );

    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

    // register main stylesheets
    wp_register_style( 'flexdev-styles', get_stylesheet_directory_uri() . '/assets/css/main.css', array(), '', 'all' );

    // adding main scripts file in the footer
    wp_register_script( 'flexdev-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );

    // adding other scripts and styles to the theme (footer) -----------------
    wp_register_script( 'flexdev-placeholder', get_stylesheet_directory_uri() . '/assets/js/placeholders.min.js', array(), '', true );
    wp_register_script( 'flexdev-scrollto', get_stylesheet_directory_uri() . '/assets/js/jquery.scrollTo.min.js', array(), '', true );
    wp_register_script( 'flexdev-svgxuse', get_stylesheet_directory_uri() . '/assets/js/svgxuse.js', array(), '', true );
    wp_register_script( 'addon-slick', get_stylesheet_directory_uri() . '/assets/js/slick.min.js', array(), '', true );
    wp_register_script( 'addon-magnificpop', get_stylesheet_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array(), '', true );
    wp_register_script( 'addon-objectfitpoly', get_stylesheet_directory_uri() . '/assets/js/ofi.browser.js', array(), '', true );
    wp_register_script( 'addon-animatedModal', get_stylesheet_directory_uri() . '/assets/js/fdt-animatedModal.js', array(), '', true );

    // enqueue HEADER styles and scripts
    wp_enqueue_style( 'flexdev-styles' );
    wp_enqueue_script( 'flexdev-modernizr' );
    wp_enqueue_script( 'flexdev-respond' );

    // enqueue FOOTER styles and scripts
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'flexdev-svgxuse' ); // enable svg <use> support in IE 11
    wp_enqueue_script( 'addon-slick' );
    wp_enqueue_script( 'addon-magnificpop' );
    wp_enqueue_script( 'addon-objectfitpoly' );
    wp_enqueue_script( 'addon-animatedModal' );
    wp_enqueue_script( 'flexdev-js' );
    wp_enqueue_script( 'flexdev-scrollto' );
  }
}

?>
