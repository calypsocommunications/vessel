<footer class="Strip  u-responsivePadding  PageFooter" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
  <div class="SectionContainer">

    <p>Need Help with Your Inventory? Visit the <a href="/contact/">Contact Us</a> Page.<br />
      Want More Info? Call <a href="tel:6034279034">603-427-9034</a> or email <a href="mailto:info@vesselconnects.com">info@vesselconnects.com</a>.</p>

      <div class="FooterNav">
        <span class="SiteCopyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</span>
        <!-- <a href="#">Privacy Policy</a>
        <a href="#">Terms &amp; Conditions</a> -->
      </div>

    </div> <!-- /container-->

    <div class="TranslateBar">
      <?php echo do_shortcode( '[GTranslate]' ); ?>
    </div>

  </footer> <!-- /footer -->

  <?php get_template_part( 'parts/modal-content' ); ?>


  <!-- all js scripts are loaded in lib/fdt-enqueues.php -->
  <?php wp_footer(); ?>

</body>

</html> <!-- end page. what a ride! -->
