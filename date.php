<?php get_header(); ?>

<div class="Strip InteriorHeader">

  <div class="InteriorHeader-top">
    <div class="SectionContainer">
      <svg class="BwLogo icon icon-VSSL-logo-1color"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-1color"></use></svg>
      <a href="/news">
        <div class="CloseModalNavButton CloseModalNavButton--text CloseModalNavButton--news">
          <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
          <span>Back</span>
        </div>
      </a>
    </div>
  </div>

  <div class="NewsBanner" style="background-image:url(<?php echo get_field('news_banner', 61); ?>">
    <span class="u-verticalCenter" style="width:100%;">
      <?php
      the_archive_title( '<h1 class="MainTitle">', '</h1>' );
      ?>
    </span>
  </div>

</div>

<div class="Strip Strip--yellowTop  NewsContent  u-responsivePadding">

  <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
    <div class="PrimaryContent">
      <div class="AjaxInitialLoad">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <article <?php post_class('BlogIntroWrap'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
              <a href="<?php the_permalink();?>"> <?php the_post_thumbnail( 'crop-news' ); ?></a>
              <h4 itemprop="headline"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
              <div class="EntryMeta">
                <span><time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('d.m.y'); ?></time></span>
                <span>, <?php the_category(', '); ?></span>
              </div> <!-- /EntryMeta -->
              <p><?php echo fdt_excerpt(120); ?><a class="MoreLink" href="<?php the_permalink();?>">Read More <svg class="icon icon-VSSL-arrow-right"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-right"></use></svg> </a></p>
            </section> <!-- /EntryContent -->
          </article> <!-- /article -->
        <?php endwhile; ?>
      </div>

      <?php

      $year = get_the_date('Y');
      $month = get_the_date('m');
      $day = get_the_date('d');
      if(is_year()){
        echo do_shortcode('[ajax_load_more post_type="post" offset="3" posts_per_page="3" max_pages="0" images_loaded="true" "' . $year . '"]');
      }
      elseif(is_month()){
        echo do_shortcode('[ajax_load_more post_type="post" offset="3" posts_per_page="3" max_pages="0" images_loaded="true" "' . $year . '" month="' . $month . '"]');
      }
      elseif(is_day()){
        echo do_shortcode('[ajax_load_more post_type="post" offset="3" posts_per_page="3" max_pages="0" images_loaded="true" "' . $year . '" month="' . $month . '" day="' . $day . '"]');
      }

      ?>

    <?php else : ?>

      <article class="PostNotFound">
        <header class="ArticleHeader">
          <h2><?php _e("Oops, Post Not Found!", "flexdev"); ?></h2>
        </header>
        <section class="EntryContent">
          <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?></p>
        </section>
        <footer class="ArticleFooter">
          <p><?php _e("This is the error message in the archive.php template.", "flexdev"); ?></p>
        </footer>
      </article>

    <?php endif; ?>

  </div> <!-- /PrimaryContent -->


</main>
</div> <!-- /Strip-->

<div class="Strip u-responsivePadding">
  <div class="SectionContainer">
    <div class="BrowseNews">
      <p>Browse News</p>
      <div class="StyledSelect">
        <?php wp_dropdown_categories( 'show_option_none=Topic' ); ?>
        <script type="text/javascript">
        <!--
        var dropdown = document.getElementById("cat");
        function onMyCatChange() {
          if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
          }
        }
        dropdown.onchange = onMyCatChange;
        -->
        </script>
      </div>

      <div class="StyledSelect">
        <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
          <option value=""><?php echo esc_attr( __( 'Date' ) ); ?></option>
          <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option' ) ); ?>
        </select>
      </div>

      <div class="LinkButton LinkButton--reversed LinkButton--reversedWhite"><a href="/news">All Articles</a></div>
    </div> <!-- /BrowseNews -->
  </div> <!-- /SectionContainer -->
</div>

<div class="Strip Strip--yellowTopThin NewsFooter">
  <div class="SectionContainer cf">
    <a href="/news">
      <div class="CloseModalNavButton--iconOnly">
        <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
      </div>
    </a>
    <span class="SiteCopyright">Copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved</span>
  </div> <!-- /SectionContainer -->
</div>

<?php get_footer(); ?>
