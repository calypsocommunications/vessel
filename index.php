<?php get_header(); ?>

  <div class="Strip">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
      <div class="PrimaryContent">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article <?php post_class('cf'); ?> role="article">

          <header class="ArticleHeader">
            <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            <div class="EntryMeta">
              <?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'boilerplate_author_bio_avatar_size', 40 ) ); ?>
              <span class="EntryMeta-author" itemprop="author" itemscope itemptype="http://schema.org/Person">By: <?php echo get_the_author(); ?></span>
              <span>Date: <time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('d.m.Y'); ?></time></span>
              <span>Categories: <?php the_category(', '); ?></span>
            </div> <!-- /EntryMeta -->
          </header> <!-- /ArticleHeader -->

          <section class="EntryContent  cf">
            <?php the_content(); ?>
          </section> <!-- /EntryContent -->

          <footer class="ArticleFooter">

            <p class="tags"><?php the_tags('<span class="tags-title">Tags:</span> ', ', ', ''); ?></p>

          </footer> <!-- /ArticleFooter -->

          <?php // comments_template(); // uncomment if you want to use them ?>

        </article> <!-- /article -->

        <?php endwhile; ?>

        <nav class="PostNav">
          <ul class="cf">
            <li class="PostNav-prev"><?php next_posts_link(__('&laquo; Older Entries', 'flexdev')) ?></li>
            <li class="PostNav-next"><?php previous_posts_link(__('Newer Entries &raquo;', 'flexdev')) ?></li>
          </ul>
        </nav> <!-- /PostNav -->

        <?php else : ?>

          <article class="PostNotFound">
            <header class="ArticleHeader">
              <h1><?php _e("Oops, Post Not Found!", "flexdev"); ?></h1>
            </header>
            <section class="EntryContent">
              <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?></p>
            </section>
            <footer class="ArticleFooter">
              <p><?php _e("This is the error message in the index.php template.", "flexdev"); ?></p>
            </footer>
          </article>

        <?php endif; ?>

      </div> <!-- /PrimaryContent -->

      <?php get_sidebar(); // sidebar ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>
