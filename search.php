<?php get_header(); ?>

<div class="Strip InteriorHeader">
  <div class="InteriorHeader-top u-responsivePadding">
    <div class="SectionContainer">
      <svg class="BwLogo icon icon-VSSL-logo-1color"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-1color"></use></svg>
      <a href="/">
        <div class="CloseModalNavButton CloseModalNavButton--text CloseModalNavButton--news">
          <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
          <span>Back</span>
        </div>
      </a>
    </div>
  </div>

  <div class="InteriorBanner" style="background-image:url(<?php bloginfo('template_url') ?>/assets/img/general_banner.jpg);">
    <h1 class="MainTitle archive-title u-verticalCenter">Search Results for: <?php echo esc_attr(get_search_query()); ?></h1>
  </div>

  <div class="Strip Strip--yellowTop SearchResultsWrap">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">


      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article <?php post_class('cf SearchResult'); ?> role="article">
          <header class="ArticleHeader">
            <h3 class="SearchTitle"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?> - <span>Read More</span></a></h3>

          </header> <!-- /ArticleHeader -->

        </article> <!-- /article -->

      <?php endwhile; ?>

      <nav class="PostNav">
        <ul class="cf">
          <li class="PostNav-prev"><?php next_posts_link(__('&laquo; Older Entries', "flexdev")) ?></li>
          <li class="PostNav-next"><?php previous_posts_link(__('Newer Entries &raquo;', "flexdev")) ?></li>
        </ul>
      </nav>

    <?php else : ?>

      <article class="PostNotFound">
        <header class="ArticleHeader">
          <h4><?php _e("Sorry, No Results.", "flexdev"); ?></h4>
        </header>
        <section class="EntryContent">
          <p><?php _e("Please try another search.", "flexdev"); ?></p>
        </section>
      </article>

    <?php endif; ?>

    <div class="SearchOptions">
      <h4>Didn't find what you were looking for?</h4>
      <div class="LinkButton"><a href="/contact">Contact Us</a></p>
    </div>

  </main>
</div> <!-- /Strip-->

  <?php get_template_part( 'parts/lowercta' ); ?>

<div class="InteriorFooter">
  <div class="SectionContainer cf">
    <a href="/">
    <div class="close-ModalContent<?php echo $modalinnercount; ?>  CloseModalNavButton CloseModalNavButton--iconOnly"> <!-- class name must match id above, ie. close-IdName -->
      <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
    </div>
  </a>
    <span class="ModalCopyright">Copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved</span>
  </div>
</div>

<?php get_footer(); ?>
