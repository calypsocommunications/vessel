<?php if( have_rows('wwd_ctas') ): ?>
  <?php $modalinnercount = 1; ?>
  <?php while( have_rows('wwd_ctas') ): the_row(); ?>

    <div id="ModalContent<?php echo $modalinnercount; ?>" class="Modalbox EntryContent">
      <div class="ModalHeader">
        <div class="SectionContainer ">
          <svg class="BwLogo icon icon-VSSL-logo-1color close-ModalContent<?php echo $modalinnercount; ?>"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-1color"></use></svg>
          <div class="close-ModalContent<?php echo $modalinnercount; ?>  CloseModalNavButton CloseModalNavButton--text "> <!-- class name must match id above, ie. close-IdName -->
            <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
            <span>Back</span>

          </div>
        </div>
      </div>
      <div class="InteriorBanner" style="background-image:url(<?php echo get_sub_field('wwd_banner_image'); ?>)">
        <span class="u-verticalCenter"><?php echo get_sub_field('wwd_title'); ?></span>
      </div>

      <div class="ModalContent">
        <div class="SectionContainer  EntryContent">
          <?php echo get_sub_field('wwd_content'); ?>
          <div class="LinkButton">
            <a href="/contact">Contact Us</a>
          </div>
        </div>
      </div>

      <div class="InteriorFooter">
        <div class="SectionContainer cf">
          <div class="close-ModalContent<?php echo $modalinnercount; ?>  CloseModalNavButton CloseModalNavButton--iconOnly"> <!-- class name must match id above, ie. close-IdName -->
            <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
          </div>
          <span class="ModalCopyright">Copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved</span>
        </div>
      </div>
    </div>
    <?php $modalinnercount++ ; ?>
  <?php endwhile; ?>
<?php endif; ?>


<?php if( have_rows('op_ctas') ): ?>
  <?php $modalinnercount = 4; ?>
  <?php while( have_rows('op_ctas') ): the_row(); ?>

    <div id="ModalContent<?php echo $modalinnercount; ?>" class="Modalbox EntryContent">
      <div class="ModalHeader">
        <div class="SectionContainer">
          <svg class="BwLogo icon icon-VSSL-logo-1color close-ModalContent<?php echo $modalinnercount; ?>"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-logo-1color"></use></svg>
          <div class="close-ModalContent<?php echo $modalinnercount; ?>  CloseModalNavButton CloseModalNavButton--text "> <!-- class name must match id above, ie. close-IdName -->
            <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
            <span>Back</span>

          </div>
        </div>
      </div>
      <div class="InteriorBanner" style="background-image:url(<?php echo get_sub_field('op_banner_image'); ?>);">
        <span class="u-verticalCenter"><?php echo get_sub_field('op_title'); ?></span>
      </div>

      <div class="ModalContent">
        <div class="SectionContainer EntryContent">
          <?php echo get_sub_field('op_content'); ?>
          <div class="LinkButton">
            <a href="/contact">Contact Us</a>
          </div>
        </div>
      </div>

      <div class="InteriorFooter">
        <div class="SectionContainer cf">
          <div class="close-ModalContent<?php echo $modalinnercount; ?>  CloseModalNavButton CloseModalNavButton--iconOnly"> <!-- class name must match id above, ie. close-IdName -->
            <svg class="icon icon-VSSL-arrow-left"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-arrow-left"></use></svg>
          </div>
          <span class="ModalCopyright">Copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved</span>
        </div>
      </div>


    </div>
    <?php $modalinnercount++ ; ?>
  <?php endwhile; ?>
<?php endif; ?>
