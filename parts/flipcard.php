<?php if( have_rows('flip_cards') ): ?>
      <br />
  <?php while( have_rows('flip_cards') ): the_row(); ?>
    <div class="CardWrapper <?php if (get_sub_field('hide_on_mobile') == "Yes" ) { ?>Hideonmobile<?php } ?>">
      <div class="Card">
        <div class="Card-face Card-front" style="background-image:url(<?php echo get_sub_field('fc_front_image'); ?>)">
          <span class="u-verticalCenter">
            <?php $icon = get_sub_field('success_icon'); ?>
            <svg class="icon icon-VSSL-Checklist"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-VSSL-<?php echo $icon; ?>"></use></svg>
          </span>
        </div>
        <div class="Card-face Card-back">
          <p> <?php echo get_sub_field('back_content'); ?> </p>
        </div>
      </div>
    </div>

  <?php endwhile; ?>
<?php endif; ?>
