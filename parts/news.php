


<div class="NewsQuery">
  <?php

  $post_object = get_field('featured_post');

  if( $post_object ):

    // override $post
    $post = $post_object;
    setup_postdata( $post );

    ?>
    <article <?php post_class('BlogIntroWrap'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

          <div class="EntryMeta">
            <span>Posted on <time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('M. d, Y'); ?></time></span>
            <span class="EntryMeta-category"><?php the_category(', '); ?></span>
          </div> <!-- /EntryMeta -->

          <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
            <h4 itemprop="headline"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
            <?php the_excerpt(); ?>
          </section> <!-- /EntryContent -->

        </article> <!-- /article -->
      <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
  <?php endif; ?>

  </div><div class="NewsLink">
    <div class="LinkButton LinkButton--reversed">
      <a href="/news">All articles</a>
    </div>
  </div>
