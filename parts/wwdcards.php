<div class="animation-element bounce-up">
  <div class="BoxLinks">
    <?php if( have_rows('wwd_ctas') ): ?>
      <?php $modalcount = 1; ?>
      <?php while( have_rows('wwd_ctas') ): the_row(); ?>


        <div class="BoxLinks-box">
          <div class="BoxLinks-box-inner">
            <a id="modal<?php echo $modalcount; ?>" href="#ModalContent<?php echo $modalcount; ?>">
              <?php
              if (get_sub_field('wwd_thumbnail')) {
                $imageArray = get_sub_field('wwd_thumbnail'); // Array returned by Advanced Custom Fields
                $imageAlt = $imageArray['alt'];
                $imageTitle = $imageArray['title'];
                $imageURL = $imageArray['url']; // Grab the full size version URL
                $imageCropURL = $imageArray['sizes']['crop-307-206']; // (sizes: thumbnail, medium, large or 'crop-size-name' as set in functions)
                // now show the image
                echo '<img src="' . $imageCropURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
              }
              ?>
              <h4 class="u-verticalCenterTransform"><?php echo get_sub_field('wwd_title'); ?></h4>
            </a>
          </div>
        </div>


        <?php $modalcount++ ;?>
      <?php endwhile; ?>

    <?php endif; ?>

  </div>
</div>
