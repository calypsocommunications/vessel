
<?php if (get_field('cta_text')) { ?>
<div class="Strip InteriorCTA" style="background-image:url(<?php echo get_field('cta_background_image'); ?>)">
  <div class="SectionContainer">
    <div class="InteriorCTA-text">
      <p>
    <?php echo get_field('cta_text'); ?>
      </p>
    </div> <!-- /class_name -->
    <div class="InteriorCTA-button LinkButton">
      <a href="<?php echo get_field('cta_button_link'); ?>"><?php echo get_field('cta_button_text'); ?></a>
    </div> <!-- /class_name -->
  </div> <!-- /SectionContainer -->
</div>
<?php } ?>
