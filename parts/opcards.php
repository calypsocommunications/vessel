<div class="animation-element bounce-up in-view">
      <div class="BoxLinks BoxLinks--centered">
        <?php if( have_rows('op_ctas') ): ?>
          <?php $modalcount = 4; ?>
          <?php while( have_rows('op_ctas') ): the_row(); ?>


            <div class="BoxLinks-box">
              <div class="BoxLinks-box-inner">
                <a id="modal<?php echo $modalcount; ?>" href="#ModalContent<?php echo $modalcount; ?>">
                  <?php
                  if (get_sub_field('op_thumbnail')) {
                    $imageArray = get_sub_field('op_thumbnail'); // Array returned by Advanced Custom Fields
                    $imageAlt = $imageArray['alt'];
                    $imageTitle = $imageArray['title'];
                    $imageURL = $imageArray['url']; // Grab the full size version URL
                    $imageCropURL = $imageArray['sizes']['crop-307-206']; // (sizes: thumbnail, medium, large or 'crop-size-name' as set in functions)
                    // now show the image
                    echo '<img src="' . $imageCropURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
                  }
                  ?>
                  <h4 class="u-verticalCenterTransform"><?php echo get_sub_field('op_title'); ?></h4>
                </a>
              </div>
            </div>


            <?php $modalcount++ ;?>
          <?php endwhile; ?>

        <?php endif; ?>
      </div>
</div>
