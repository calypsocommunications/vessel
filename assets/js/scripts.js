/**
 * FlexDev Theme Scripts File **************
 * This file contains any js scripts you want added to the theme. Instead of calling it in the header or throwing
 * it inside wp_head() this file will be called automatically in the footer so as not to slow the page load.
 */



/* JQUERY VIEWPORT RE-SIZING? --> SEE HELPERS/RESPONSIVE MISC */


jQuery(document).ready(function ($) {
// *********************** START CUSTOM SCRIPTS *******************************
//

//Hero Slider

$('.HeroSlider').slick({
   dots: false,
   arrows: false,
   fade: true,
   autoplay: true,
   autoplaySpeed: 5000,
   speed: 1000,
   pauseOnHover: false
});


  // modal menu init
  var modal_menu = jQuery("#modal1").animatedModal({
    modalTarget: 'ModalContent1',
    animatedIn: 'zoomIn',
    animatedOut: 'bounceOut',
    animationDuration: '0.50s',
    color: '#32323a'
  });

  // modal menu init
  var modal_menu = jQuery("#modal2").animatedModal({
    modalTarget: 'ModalContent2',
    animatedIn: 'zoomIn',
    animatedOut: 'bounceOut',
    animationDuration: '0.50s',
    color: '#32323a'
  });

  // modal menu init
var modal_menu = jQuery("#modal3").animatedModal({
  modalTarget: 'ModalContent3',
  animatedIn: 'zoomIn',
  animatedOut: 'bounceOut',
  animationDuration: '0.50s',
  color: '#32323a'
});

// modal menu init
var modal_menu = jQuery("#modal4").animatedModal({
modalTarget: 'ModalContent4',
animatedIn: 'zoomIn',
animatedOut: 'bounceOut',
animationDuration: '0.50s',
color: '#32323a'
});

// modal menu init
var modal_menu = jQuery("#modal5").animatedModal({
modalTarget: 'ModalContent5',
animatedIn: 'zoomIn',
animatedOut: 'bounceOut',
animationDuration: '0.50s',
color: '#32323a'
});

// close modal menu if esc key is used
// jQuery(document).keyup(function(ev){
//   if(ev.keyCode == 27) {
//     modal_menu.close();
//   }
// });




  // objectfit polyfill
  $(function () { objectFitImages('.u-polyFit'); });


  //Sticky menu
  $(window).scroll(function() {
  if ($(this).scrollTop() > 1){
      $('.PageHeader').addClass("sticky");
      // $('#PopdownMenu').addClass("ShowPopdown");
    }
    else{
      $('.PageHeader').removeClass("sticky");
      $('#PopdownMenu').removeClass("ShowPopdown");
      $('#StickyMenuLink-menu').removeClass( "Hide" );
      $('#StickyMenuLink-menu').addClass( "Show" );
      $('#StickyMenuLink-x').removeClass( "Show" );
      $('#StickyMenuLink-x').addClass( "Hide" );
    }
  });

  //menu popdown
  $( "#StickyMenuLink span" ).click(function() {
    $( "#PopdownMenu" ).toggleClass( "ShowPopdown" );
    $( "#StickyMenuLink span" ).toggleClass( "Hide Show" );
  });

  //Scrollto
  //From main nav
  $('.PageHeader a[href^="#"]').live("click", function(e){
    // Prevent the jump and the #hash from appearing on the address bar
      e.preventDefault();
      // Scroll the window, stop any previous animation, stop on user manual scroll
      // Check https://github.com/flesler/jquery.scrollTo for more customizability
      $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:false, offset:-75});
   });

  //From sticky nav
$('.sticky a[href^="#"]').live("click", function(e){
  // Prevent the jump and the #hash from appearing on the address bar
    e.preventDefault();
    // Scroll the window, stop any previous animation, stop on user manual scroll
    // Check https://github.com/flesler/jquery.scrollTo for more customizability
    $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:false, offset:-165});
 });


 //From hero arrow
$('.HeroBanner-arrow a[href^="#"]').live("click", function(e){
// Prevent the jump and the #hash from appearing on the address bar
  e.preventDefault();
  // Scroll the window, stop any previous animation, stop on user manual scroll
  // Check https://github.com/flesler/jquery.scrollTo for more customizability
  $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:false, offset:-75});
});


//QA Slider

$('.QASlider').slick({
   dots: true
});


$('.LogoWall').slick({
   arrows: false,
   slidesToShow: 13,
   responsive: [
       {
         breakpoint: 1000,
         settings: {
           arrows: false,
           slidesToShow: 4,
           autoplay: true,
           autoplaySpeed: 2000
         }
       }
     ]
});

// slidein animation
//Cache reference to window and animation items
var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');

// POPUP SEARCH ***********************************************************

$('.magnific-search a').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',
    closeMarkup: '<div class="form-close-button"><button title="%title%" type="button" class="mfp-close">X</button></div>',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#search';
				}
			}
		}
	});



// *********************** END CUSTOM SCRIPTS *********************************

}); /* end of DOM ready scripts */
