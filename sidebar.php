      <div class="SecondaryContent" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">

        <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
          <?php dynamic_sidebar( 'sidebar' ); ?>
        <?php endif; ?>

      </div> <!-- /SecondaryContent -->
